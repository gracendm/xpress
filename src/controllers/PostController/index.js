import fs from 'fs'
import path from 'path'
import { Op } from 'sequelize'
import data from '../../data/data.json'
import { Post } from '../../models'

const filePath = path.resolve(__dirname, '../../data/data.json')

class PostController {
  static get = async (req, res) => {
    try {
      // const { Post } = models

      const options = {}

      const { title, sort } = req.query

      // Sort
      if (sort) {
        options.order = [['id', sort]]
      }

      // Search by title
      if (title) {
        options.where = {
          title: {
            [Op.like]: `%${title}%`,
          },
        }
      }

      const filteredData = await Post.findAll(options)

      return res.status(200).json({ data: filteredData })
    } catch (error) {
      console.log(error);
      return res.status(500).json({ message: 'server error' })
    }
  }

  static getIndexView = (req, res) => {
    res.render(
      'posts',
      { user: 'Alfian' },
    )
  }

  static create = async (req, res) => {
    try {
      const {
        title, body, authorId,
      } = req.body

      // const post = await Post.findOne({ where: { id } });

      // if (post) {
      //   return res.status(400).json({ message: 'id is already exist' })
      // }

      const create = await Post.create({
        authorId,
        body,
        title,
      })

      return res.status(200).json({ message: 'creating id success' })
    } catch (error) {
      console.log(error)

      return res.status(500).json({ message: 'server error' })
    }
  }

  static update = async (req, res) => {
    try {
      const { id } = req.params
      const post = await Post.findOne({ where: { id } });

      if (!post) {
        return res.status(400).json({ message: 'id is not exist' })
      }

      const { title, author } = req.body

      await Post.update({ title }, {
        where: { id },
      })

      return res.status(201).json({ message: 'successfully updated' })
    } catch (error) {
      console.log(error)

      return res.status(500).json({ message: 'server error' })
    }
  }

  static delete = async (req, res) => {
    const { id } = req.params
    const post = await Post.findOne({ where: { id } });

    if (!post) {
      return res.status(404).json({ message: '404! Id is not exist.' })
    }

    await Post.destroy({
      where: { id },
    });

    return res.status(201).json({ message: 'successfully deleted' })
  }
}

export default PostController
