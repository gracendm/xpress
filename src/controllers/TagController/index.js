import fs from 'fs'
import path from 'path'
import { Op } from 'sequelize'
import data from '../../data/data.json'
import { Tag } from '../../models'

const filePath = path.resolve(__dirname, '../../data/data.json')

class TagController {
  static get = async (req, res) => {
    try {

      const options = {}

      const { name, sort } = req.query

      // Sort
      if (sort) {
        options.order = [['id', sort]]
      }

      // Search by name
      if (name) {
        options.where = {
          name: {
            [Op.like]: `%${name}%`,
          },
        }
      }

      const filteredData = await Tag.findAll(options)

      return res.status(200).json({ data: filteredData })
    } catch (error) {
      console.log(error);
      return res.status(500).json({ message: 'server error' })
    }
  }

  static getIndexView = (req, res) => {
    res.render(
      'tags',
      { user: 'Alfian' },
    )
  }

  static create = async (req, res) => {
    try {
      const {
        name,
      } = req.body

    
      const create = await Tag.create({
        name,
      })

      return res.status(200).json({ message: 'creating id success' })
    } catch (error) {
      console.log(error)

      return res.status(500).json({ message: 'server error' })
    }
  }

  static update = async (req, res) => {
    try {
      const { id } = req.params
      const tag = await Tag.findOne({ where: { id } });

      if (!tag) {
        return res.status(400).json({ message: 'id is not exist' })
      }

      const { name, author } = req.body

      await Tag.update({ name }, {
        where: { id },
      })

      return res.status(201).json({ message: 'successfully updated' })
    } catch (error) {
      console.log(error)

      return res.status(500).json({ message: 'server error' })
    }
  }

  static delete = async (req, res) => {
    const { id } = req.params
    const tag = await Tag.findOne({ where: { id } });

    if (!tag) {
      return res.status(404).json({ message: '404! Id is not exist.' })
    }

    await Tag.destroy({
      where: { id },
    });

    return res.status(201).json({ message: 'successfully deleted' })
  }
}

export default TagController
