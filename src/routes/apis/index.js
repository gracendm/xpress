import express from 'express'
import Middleware from '../../middlewares'
import PostValidator from '../../validators/PostValidator'
import PostController from '../../controllers/PostController'
import AuthorController from '../../controllers/AuthorController'
import TagController from '../../controllers/TagController'

const router = express.Router()

router.get('/posts', PostController.get)
router.post('/posts', PostController.create)
router.patch('/posts/:id', PostController.update)
router.delete('/posts/:id', PostController.delete)

router.get('/authors', AuthorController.get)
router.post('/authors', AuthorController.create)
router.patch('/authors/:id', AuthorController.update)
router.delete('/authors/:id', AuthorController.delete)

router.get('/tags', TagController.get)
router.post('/tags', TagController.create)
router.patch('/tags/:id', TagController.update)
router.delete('/tags/:id', TagController.delete)

export default router
