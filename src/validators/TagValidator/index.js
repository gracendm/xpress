import { body, param } from 'express-validator'

class TagValidator {
  static create = () => [
    body('name').exists().isString(),
  ]

  static update = () => [
    param('id').exists().isUUID(),
    body('name').optional().isString(),
  ]

  static delete = () => [
    param('id').exists().isUUID(),
  ]
}

export default TagValidator
